import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Usuario} from '../interfaz/usuario';

@Injectable()
export class GlobalDataService {

   private messageSource = new BehaviorSubject('default message');
   currentMessage = this.messageSource.asObservable();

   private userData = new BehaviorSubject(null);
   currentUserData = this.userData.asObservable();



   constructor() {
      if (localStorage.getItem('user_data')) {
         this.changeUserData(JSON.parse(localStorage.getItem('user_data')));
      }
   }



   changeMessage(message: string) {
      this.messageSource.next(message);
   }


   changeUserData(user: object) {
      this.userData.next(user);
   }

}
