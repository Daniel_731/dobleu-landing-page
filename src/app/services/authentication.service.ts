import {Injectable} from '@angular/core';
import {Usuario} from '../interfaz/usuario';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';


@Injectable({
   providedIn: 'root'
})
export class AuthenticationService {

   constructor(private angularFireAuth: AngularFireAuth, private angularFirestore: AngularFirestore) {}




   async loginNormal(user: Usuario) {
      try {

         return await this.angularFireAuth.auth.signInWithEmailAndPassword(user.usuario_email, user.usuario_clave);

      } catch (error) {
         console.log(error);
      }

      return null;
   }



   async registerUser(user: Usuario) {
      try {

         return await this.angularFireAuth.auth.createUserWithEmailAndPassword(user.usuario_email, user.usuario_clave);

      } catch (error) {
         console.log(error);
      }

      return null;
   }



   async createUserTable(table: string, datos: object) {
      try {

         return this.angularFirestore.collection(`${table}`).add(datos);

      } catch (error) {
         console.log(error);
      }

      return null;
   }



   /*
   async readResponse(table: string, id: any) {
      try {

         return this.angularFirestore.collection(`${table}`, ref => ref
            .where('usuario_codigo', '==', id)
         ).valueChanges();

      } catch (error) {
         console.log(error);
      }

      return null;
   }
   */



   async loginBranchOffice(table: string, usuario: any, clave: any) {
      try {

         return this.angularFirestore.collection(`${table}`, ref => ref
            .where('sucursal_usuario', '==', usuario)
            .where('sucursal_clave', '==', clave)
            .where('sucursal_estado', '==', 'ACTIVO')
         ).valueChanges();

      } catch (error) {
         console.log(error);
      }

      return null;
   }

}
