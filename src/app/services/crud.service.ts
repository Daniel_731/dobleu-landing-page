import {Injectable} from '@angular/core';

import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireStorage} from '@angular/fire/storage';

// import { AngularFireStorage } from '@angular/fire/storage';
import {switchMap, scan, takeWhile, startWith, mapTo, finalize} from 'rxjs/operators';
import {Subject, Observable} from 'rxjs';


@Injectable({
   providedIn: 'root'
})
export class CrudService {

   constructor(
      private fireDatabase: AngularFirestore,
      // private imagenFirebase: AngularFireStorage
   ) {
   }

   /**
    * @param table
    * @param datos
    * @eder
    * Metodo general para crear los registros en la bd
    */
   async createDatos(table: string, datos: object) {
      return this.fireDatabase.collection(`${table}`).add(datos);
   }

   /**
    * @param table
    * @param datos
    * @eder
    * Este metodo trae todos los datos de una unica tabla
    */

   async read(table: string) {

      return this.fireDatabase.collection(`${table}`).valueChanges(respue => {
         return respue.map(a => {
            return {id: a.payload.doc.id, ...a.payload.doc.data()} as any;
         });
      });
   }

   /**
    * @param table
    * @param datos
    * @eder
    * Este metodo es para actualizar los registros se agrega un parametro mas
    */

   async update(table: string, value: any, datos: object) {
      this.fireDatabase.doc(`${table}/${value}`).update(datos);
   }

   /**
    * @param table
    * @param datos
    * @eder
    * Este metodo es para eliminar datos de una bd
    */
   async delete(table: string, id: any) {
      this.fireDatabase.doc(`${table}/${id}`).delete();
   }

   /**
    * @param table
    * @param datos
    * @eder
    * Este metodo trae uno o varios registros que tengan asociado a un campo a buscar
    */

   async readGeneral(tabla: string, campo: any, value: any) {
      return this.fireDatabase.collection(`${tabla}`, ref => ref
         .where(campo, '==', `${value}`)
      ).snapshotChanges();
   }

   async readGeneral1(tabla: string, campo: any, value: any) {
      return this.fireDatabase.collection(`${tabla}`, ref => ref
         .where(campo, '==', value)
      ).valueChanges();
   }

   async readGeneral2(tabla: string, campo: any, value: any) {
      console.log(tabla, campo, value);
      return this.fireDatabase.collection(`${tabla}`, ref => ref
         .where('categoria_codigo', '==', value)
      ).valueChanges();
      // console.log(algo);
      // algo.subscribe(res => {
      //   console.log(res);
      // })
   }


   async readUsuarios(table: string, campo: any, id: any) {

      return this.fireDatabase.collection(`${table}`, ref => ref
         .where(campo, '==', `${id}`))
         .snapshotChanges();


   }


}
