import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Usuario} from '../../interfaz/usuario';
import {GlobalDataService} from '../../services/global-data.service';
import {Router} from '@angular/router';
import {CrudService} from '../../services/crud.service';
import {Contacto} from '../../interfaz/contacto';


@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

   user: Usuario;
   showIncorrectCredentials = false;
   userData: object = null;
   tablaUser = '/tb_usuario';
   usuarioLocal: any;
   campo = 'usuario_codigo';
   local: any;

   alertParameters: object;

   contactForm: Contacto;

   @ViewChild('form_login_input_email', {static: false}) formLoginInputEmail: ElementRef;
   @ViewChild('form_login_close_modal', {static: false}) formLoginCloseModal: ElementRef;


   constructor(
      private autenticacionService: AuthenticationService,
      private globalDataService: GlobalDataService,
      private crudService: CrudService,
      private router: Router) {

      this.local = JSON.parse(localStorage.getItem('user_data'));
      console.log(this.local);
   }


   ngOnInit(): void {
      this.user = {
         usuario_email: null,
         usuario_clave: null
      };

      this.globalDataService.currentUserData.subscribe(value => this.userData = value);

      this.alertParameters = {
         type_alert: null,
         show: false,
         message: null
      };

      this.contactForm = {
         contacto_titulo: null,
         contacto_email: null,
         contacto_contenido: null
      };
   }


   /* loginNormal() {
       try {

          this.autenticacionService.loginNormal(this.user).then(value => {
             if (value && value[`user`]) {
                console.log(value[`user`]);

                localStorage.setItem('user_data', JSON.stringify(value[`user`]));
                this.globalDataService.changeUserData(value[`user`]);

                this.formLoginCloseModal.nativeElement.click();

             } else {



                this.loginBranchOffice();
             }
          });

       } catch (e) {
          console.error(e);
       }
    }*/

   async loginNormal() {
      await this.autenticacionService.loginNormal(this.user).then(user => {
         if (user) {
            this.user.usuario_codigo = user.user.uid;

            this.crudService.readUsuarios(this.tablaUser, this.campo, this.user.usuario_codigo).then(respuesta => {
               respuesta.subscribe(res => {
                  res.forEach(async value => {
                     const data = res[0].payload.doc.data();
                     this.user = data as Usuario;

                     localStorage.setItem('user_data', JSON.stringify(this.user));

                     this.globalDataService.changeUserData(this.user);
                     window.location.reload();
                     this.formLoginCloseModal.nativeElement.click();
                  });
               });
            });

         } else {
            this.loginBranchOffice();
         }

      }).catch(error => {
         console.log(error);
      });
   }


   loginBranchOffice() {
      // debugger;

      this.autenticacionService.loginBranchOffice('tb_sucursal', this.user.usuario_email, this.user.usuario_clave).then(respuesta => {
         respuesta.subscribe(async ressucursal => {
            if (ressucursal.length > 0) {

               this.user.usuario_tipo = 'Sucursal';
               this.user.usuario_codigo = ressucursal[0][`usuario_codigo`];
               this.user.usuario_nombre = ressucursal[0][`sucursal_nombre`];
               this.user.usuario_imagen = ressucursal[0][`sucursal_imagen`];
               this.user.categoria_codigo = ressucursal[0][`categoria_codigo`];
               this.user.codigo_sucursal = ressucursal[0][`sucursal_codigo`];

               localStorage.setItem('user_data', JSON.stringify(this.user));
               this.globalDataService.changeUserData(this.user);
               window.location.reload();

               this.formLoginCloseModal.nativeElement.click();

            } else {

               this.user.usuario_clave = null;
               this.formLoginInputEmail.nativeElement.focus();
               this.showIncorrectCredentials = true;
            }

         });
      });
   }


   logOut() {
      try {

         localStorage.removeItem('user_data');

         this.globalDataService.changeUserData(null);
         window.location.reload();
         this.router.navigateByUrl('/home');

         this.user = {
            usuario_email: null,
            usuario_clave: null
         };

      } catch (e) {
         console.error(e);
      }
   }



   saveContactForm() {
      try {

         this.crudService.createDatos('tb_contacto', this.contactForm).then(value => {
            this.showAlert('success', 'Gracias por ponerte en contacto con nosotros, pronto responderemos tus inquietudes!');
         }).catch(reason => {
            console.error(reason);
         });

      } catch (error) {
         console.error(error);
      }
   }



   showAlert(typeAlert: 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info' | 'light' | 'dark', messages: string) {
      try {

         this.alertParameters = {
            type_alert: typeAlert,
            show: true,
            message: messages
         };

         this.hideAlert();

      } catch (error) {
         console.error(error);
      }
   }



   hideAlert() {
      try {

         setTimeout(() => {
            this.alertParameters = {
               type_alert: null,
               show: false,
               message: null
            };
         }, 5000);

      } catch (error) {
         console.error(error);
      }
   }

}
