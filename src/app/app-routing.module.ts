import {NgModule, Component} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {PlayComponent} from './pages/play/play.component';
import {DownloadComponent} from './pages/download/download.component';
import {BienvenidosComponent} from './pages/bienvenidos/bienvenidos.component';
import {ContactoComponent} from './pages/contacto/contacto.component';
import {LoginComponent} from './pages/login/login.component';
import {SucursalComponent} from './pages/sucursal/sucursal.component';
import {AnuncioComponent} from './pages/anuncio/anuncio.component';
import {ListarSucursalComponent} from './pages/listar-sucursal/listar-sucursal.component';
import {UsuarioComponent} from './pages/usuario/usuario.component';
import {ListarAnuncioComponent} from './pages/listar-anuncio/listar-anuncio.component';
import {TermsAndConditionsComponent} from './pages/terms-and-conditions/terms-and-conditions.component';
import {ImportExcelComponent} from './pages/import-excel/import-excel.component';


const routes: Routes = [
   // Welcome
   {
      path: '',
      component: BienvenidosComponent
   },
   {
      path: 'register',
      component: BienvenidosComponent
   },

   // Home
   {
      path: 'home',
      component: HomeComponent
   },

   // Play
   {
      path: 'play',
      component: PlayComponent
   },

   // Download
   {
      path: 'download',
      component: DownloadComponent
   },
   {
      path: 'contacto',
      component: ContactoComponent
   },
   {
      path: 'anuncio',
      component: AnuncioComponent
   },
   {
      path: 'anuncio/:id',
      component: AnuncioComponent
   },
   {
      path: 'listar-anuncio',
      component: ListarAnuncioComponent
   }
   ,
   {
      path: 'sucursal',
      component: SucursalComponent
   },
   {
      path: 'sucursal/:id',
      component: SucursalComponent
   },
   {
      path: 'listar-sucursales',
      component: ListarSucursalComponent
   },

   {
      path: 'login',
      component: LoginComponent
   },
   {
      path: 'usuario',
      component: UsuarioComponent
   },

   // Terms and conditions
   {
      path: 'terms-and-conditions',
      component: TermsAndConditionsComponent
   },

   // Import excel
   {
      path: 'import-excel',
      component: ImportExcelComponent
   }
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
})

export class AppRoutingModule {
}
