import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';

import {GlobalDataService} from './services/global-data.service';

import {BienvenidosComponent} from './pages/bienvenidos/bienvenidos.component';
import {HeaderComponent} from './components/header/header.component';
import {HomeComponent} from './pages/home/home.component';
import {PlayComponent} from './pages/play/play.component';
import {DownloadComponent} from './pages/download/download.component';
import {ContactoComponent} from './pages/contacto/contacto.component';
import {LoginComponent} from './pages/login/login.component';
import {AngularFireStorageModule} from '@angular/fire/storage';


/**
 * @ multiple select
 */

import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown-angular7';


// Firebase
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {SucursalComponent} from './pages/sucursal/sucursal.component';
import {AnuncioComponent} from './pages/anuncio/anuncio.component';
import {ListarSucursalComponent} from './pages/listar-sucursal/listar-sucursal.component';
import {UsuarioComponent} from './pages/usuario/usuario.component';
import {ListarAnuncioComponent} from './pages/listar-anuncio/listar-anuncio.component';
import {TermsAndConditionsComponent} from './pages/terms-and-conditions/terms-and-conditions.component';
import {ImportExcelComponent} from './pages/import-excel/import-excel.component';

// import { AngularFireModule } from '@angular/fire';

@NgModule({
   declarations: [
      AppComponent,
      BienvenidosComponent,
      HeaderComponent,
      HomeComponent,
      PlayComponent,
      DownloadComponent,
      ContactoComponent,
      LoginComponent,
      SucursalComponent,
      AnuncioComponent,
      ListarSucursalComponent,
      UsuarioComponent,
      ListarAnuncioComponent,
      TermsAndConditionsComponent,
      ImportExcelComponent
   ],

   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      AngularFireModule.initializeApp(environment.firebase),
      AngularFirestoreModule,
      AngularFireAuthModule,
      NgMultiSelectDropDownModule.forRoot(),
      AngularFireStorageModule
   ],

   providers: [
      GlobalDataService
   ],

   bootstrap: [
      AppComponent
   ]
})

export class AppModule {
}
