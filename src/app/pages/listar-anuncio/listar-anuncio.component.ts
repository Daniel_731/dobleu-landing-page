import {Component, OnInit} from '@angular/core';
import {CrudService} from 'src/app/services/crud.service';
import {Producto} from 'src/app/interfaz/producto';

@Component({
   selector: 'app-listar-anuncio',
   templateUrl: './listar-anuncio.component.html',
   styleUrls: ['./listar-anuncio.component.scss']
})
export class ListarAnuncioComponent implements OnInit {

   tablaProducto = '/tb_producto';
   campo = 'sucursal_codigo';
   listarAnuncio: Array<Producto>;
   valor: any;
   local: any;
   updateProducto: Producto;




   constructor(private crudService: CrudService) {}




   ngOnInit() {

      this.local = JSON.parse(localStorage.getItem('user_data'));
      console.log(this.local);
      this.listarMisAnuncios(this.local.usuario_codigo);

      this.updateProducto = {
         producto_activo: null
      };

   }


   async listarMisAnuncios(value) {

      if (this.local.usuario_tipo === 'Empresa') {
         this.campo = 'usuario_codigo';
         this.valor = this.local.usuario_codigo;
         console.log(this.campo);
      } else {
         this.valor = this.local.codigo_sucursal;
      }

      console.log(this.tablaProducto, this.campo, this.valor);

      try {
         this.crudService.readGeneral1(this.tablaProducto, this.campo, this.valor).then(respuesta => {
            respuesta.subscribe(suscr => {
               console.log(suscr);
               this.listarAnuncio = suscr as Array<Producto>;
               console.log(this.listarAnuncio);
            });
         });
      } catch (error) {
         console.log('error en el catch ' + JSON.stringify(error));
      }
   }

}
