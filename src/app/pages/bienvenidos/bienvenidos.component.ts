import {Component, OnInit} from '@angular/core';
import {Usuario} from '../../interfaz/usuario';
import {UsuarioCategoria} from '../../interfaz/usuario-categoria';
import {CrudService} from '../../services/crud.service';
import {Router} from '@angular/router';

import {AngularFireAuth} from '@angular/fire/auth';

import Swal from 'sweetalert2';
import {AuthenticationService} from 'src/app/services/authentication.service';

import {GlobalDataService} from '../../services/global-data.service';


@Component({
   selector: 'app-bienvenidos',
   templateUrl: './bienvenidos.component.html',
   styleUrls: ['./bienvenidos.component.scss']
})
export class BienvenidosComponent implements OnInit {

   dropdownList = [];
   selectedItems = [];
   dropdownSettings = {};

   tablaUsuario = '/tb_usuario';
   tablaCategoria = '/tb_categoria';
   tablaDetalleUsu = 'tb_usudetalle';
   detalleUsuario: UsuarioCategoria;
   usuario: Usuario;

   constructor(
      private crudService: CrudService,
      public afAuth: AngularFireAuth,
      public authService: AuthenticationService,
      private router: Router,
      private globalDataService: GlobalDataService,
   ) {}

   ngOnInit() {
      console.log(new Date().toISOString().slice(0, 10)+'/'+new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds());
      this.dropdownSettings = {
         singleSelection: false,
         idField: 'categoria_codigo',
         textField: 'categoria_nombre',
         itemsShowLimit: 3,
         allowSearchFilter: true
      };


      this.usuario = {
         usuario_alias: null,
         usuario_celular: null,
         usuario_clave: null,
         usuario_codigo: null,
         usuario_coleccion: null,
         usuario_direccion: null,
         usuario_email: null,
         usuario_fechamodificacion: null,
         usuario_nombre: null,
         usuario_telefono: null,
         usuario_fechacreacion: new Date().toISOString().slice(0, 10)+'/'+new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds(),
         usuario_tipologin: 'Correo',
         usuario_tipo: 'Empresa',
         usuario_documento: null,
      };

      this.listarCategorias();

   }

   onItemSelect(item: any) {
      console.log(item);
      // this.crearCategorias();
   }

   onSelectAll(items: any) {
      console.log(items);
      // this.crearCategorias();
   }

   async listarCategorias() {
      this.crudService.read(this.tablaCategoria).then(respuesta => {
         respuesta.subscribe(suscr => {
            console.log(suscr);
            this.dropdownList = suscr;
         });

      }).catch(error => {
         console.log('error en a respuesta ' + JSON.stringify(error));
      });
   }

   async crearCategorias(value) {

      console.log(this.selectedItems);

      this.selectedItems.forEach(respuesta => {

         setTimeout(() => {

            respuesta[`usudet_codigo`] = new Date().getTime() + new Date().getUTCMilliseconds();
            respuesta[`usuario_codigo`] = value;
            respuesta[`categoria_codigo`] = respuesta[`categoria_codigo`];
            try {
               this.crudService.createDatos(this.tablaDetalleUsu, respuesta).then(respue => {
                  console.log('detalle ready');
               }).catch(error => {

               });
            } catch (error) {

            }
         }, 3);
      });
   }


   async crearUsuario() {
      await this.authService.registerUser(this.usuario).then(async respuestas => {

         try {
            this.usuario.usuario_codigo = respuestas.user.uid;
            this.authService.createUserTable(this.tablaUsuario, this.usuario).then(async respuesta => {

               // localStorage.setItem('informacion_usuario', JSON.stringify(this.usuario));

               localStorage.setItem('user_data', JSON.stringify(this.usuario));
               this.globalDataService.changeUserData(this.usuario);

               Swal.fire('Correcto!',
                  'Se registro Satisfactoriamente',
                  'success');
               await this.crearCategorias(this.usuario.usuario_codigo);
               window.location.reload();
               this.router.navigateByUrl('/home');

            }).catch(error => {
               console.log('Error en la consulta ' + JSON.stringify(error));
            });
         } catch (error) {

            Swal.fire({
               type: 'error',
               title: 'Oops...',
               text: 'Ya existe un usuario con este correo!',
               footer: `<a href>${error}</a>`
            });

            console.log('error en el catch' + JSON.stringify(error));
         }
      });

   }


   async mensajeAlertas() {

      Swal.fire({
         title: '¿Validar tus datos?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Registrarte!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

             this.crearUsuario();

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'El registro fue cancelado :)',
               'error'
            );
         }
      });
   }

}
