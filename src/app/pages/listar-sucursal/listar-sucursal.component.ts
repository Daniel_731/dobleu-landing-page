import {Component, OnInit} from '@angular/core';
import {Sucursal} from '../../interfaz/sucursal';
import {CrudService} from 'src/app/services/crud.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
   selector: 'app-listar-sucursal',
   templateUrl: './listar-sucursal.component.html',
   styleUrls: ['./listar-sucursal.component.scss']
})
export class ListarSucursalComponent implements OnInit {

   listarSucursales: Array<Sucursal>;
   tablaSucursal = '/tb_sucursal';
   campoSucursal = 'sucursal_codigo';
   idColeccion: any;
   updateSucursal: Sucursal;
   local: any;
   campoUsuario = 'usuario_codigo';




   constructor(private crudService: CrudService) {}




   ngOnInit() {
      this.local = JSON.parse(localStorage.getItem('user_data'));
      console.log(this.local);

      this.listarMisSucursales(this.local.usuario_codigo);

      this.updateSucursal = {
         sucursal_estado: null,
      };
   }



   async listarMisSucursales(value) {
      try {
         this.crudService.readGeneral1(this.tablaSucursal, this.campoUsuario, value).then(respuesta => {
            respuesta.subscribe(suscr => {
               console.log(suscr);
               this.listarSucursales = suscr as Array<Sucursal>;
               console.log(this.listarSucursales);
            });
         });
      } catch (error) {
         console.log('error en el catch ' + JSON.stringify(error));
      }
   }



   async activarSucursal(sucursal) {

      this.crudService.readGeneral(this.tablaSucursal, this.campoSucursal, sucursal).then(repuesta => {
         repuesta.subscribe(res => {
            this.idColeccion = res[0].payload.doc.id;

            this.updateSucursal.sucursal_estado = 'ACTIVA';

            this.crudService.update(this.tablaSucursal, this.idColeccion, this.updateSucursal)
               .then(respuesta => {

                  Swal.fire('Correcto!',
                     'La sucursal se Inactivo Satisfactoriamente',
                     'success');

               }).catch(error => {
               Swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Valida los datos por favor!',
                  footer: `<a href>${error}</a>`
               });
            });

         });
      });
      console.log(sucursal);
   }



   async inactivaSucursal(sucursal) {
      this.crudService.readGeneral(this.tablaSucursal, this.campoSucursal, sucursal).then(repuesta => {
         repuesta.subscribe(res => {
            this.idColeccion = res[0].payload.doc.id;

            this.updateSucursal.sucursal_estado = 'INACTIVO';

            this.crudService.update(this.tablaSucursal, this.idColeccion, this.updateSucursal)
               .then(respuesta => {

                  Swal.fire('Correcto!',
                     'La sucursal se Inactivo Satisfactoriamente',
                     'success');

               }).catch(error => {
               Swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Valida los datos por favor!',
                  footer: `<a href>${error}</a>`
               });
            });

         });
      });
      console.log(sucursal);
   }



   async mensajeAlertasActivar(values) {
      Swal.fire({
         title: '¿Se Activara la sucursal?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Aceptar!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

            this.activarSucursal(values);

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'El registro fue cancelado :)',
               'error'
            );
         }
      });
   }



   async mensajeAlertas(values) {

      Swal.fire({
         title: '¿Se inactivara la sucursal?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Aceptar!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

            this.inactivaSucursal(values);

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'El registro fue cancelado :)',
               'error'
            );
         }
      });
   }

}
