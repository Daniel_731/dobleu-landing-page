import {Component, OnInit} from '@angular/core';
import {Producto} from 'src/app/interfaz/producto';
import {CrudService} from 'src/app/services/crud.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularFireStorage} from '@angular/fire/storage';
import Swal from 'sweetalert2';
import {Usuario} from '../../interfaz/usuario';
import {finalize} from 'rxjs/operators';

@Component({
   selector: 'app-anuncio',
   templateUrl: './anuncio.component.html',
   styleUrls: ['./anuncio.component.scss']
})
export class AnuncioComponent implements OnInit {

   crearAnuncio: Producto;
   tablaProducto = '/tb_producto';
   local: Usuario;
   imagenPerfil: any;
   rutaImagen: any;
   obtenerurlImagen: any;
   rutaImagenFi: any;
   idParametro: any;
   idColeccion: any;
   listadoMiCategorias: any;




   constructor(
      private crudService: CrudService,
      private router: Router,
      private parametro: ActivatedRoute,
      private imagenFirebase: AngularFireStorage) {

      this.idParametro = this.parametro.snapshot.paramMap.get('id');

      if (this.idParametro) {
         this.mostarrAnuncio();
      }
   }




   ngOnInit() {

      this.local = JSON.parse(localStorage.getItem('user_data'));

      this.mostrarCategoria();

      this.crearAnuncio = {
         producto_codigo: null,
         producto_nombre: null,
         producto_descripcion: null,
         producto_descuento: null,
         producto_imagen: null,
         producto_precioahora: null,
         producto_precioantes: null,
         sucursal_codigo: null,
         categoria_codigo: null,
         usuario_codigo: null,
         producto_activo: '',
      };
   }



   async mostrarCategoria() {
      this.crudService.readGeneral2('/tb_categoria', 'categoria_codigo', this.local.categoria_codigo)
         .then(respuesta => {
            respuesta.subscribe(async suscr => {
               this.listadoMiCategorias = suscr[0][`categoria_descripcion`];
            });

         }).catch(error => {
      });
   }



   async mostarrAnuncio() {
      this.crudService.readGeneral(this.tablaProducto, 'producto_codigo', this.idParametro)
         .then(respuesta => {
            respuesta.subscribe(susc => {
               if (susc) {
                  this.idColeccion = susc[0].payload.doc.id;
                  this.crearAnuncio = susc[0].payload.doc.data();
               }
            });
         }).catch(error => {});
   }



   uploadAvatar() {
      document.getElementById('upload-avatar').click();
   }



   async subirFotos(event) {
      this.imagenPerfil = event.target.files[0];
      this.rutaImagen = `anuncios/${Date.now() + new Date().getUTCMilliseconds().toString()}`;
      const ref = this.imagenFirebase.ref(this.rutaImagen);
      const task = this.imagenFirebase.upload(this.rutaImagen, this.imagenPerfil);

      task.snapshotChanges().pipe(
         finalize(async () => {

            ref.getDownloadURL().subscribe(value => {
               this.crearAnuncio.producto_imagen = value;
            });

            // this.obtenerurlImagen = await ref.getDownloadURL();

            // console.log(this.obtenerurlImagen);
         })
      ).subscribe();

      /*
      task.snapshotChanges().pipe(() => this.obtenerurlImagen = ref.getDownloadURL()).subscribe(respuestas => {
         this.rutaImagenFi = respuestas;
         this.crearAnuncio.producto_imagen = this.rutaImagenFi;
      });
      */
   }



   async crearAnuncios() {
      const valor = this.crearAnuncio.producto_precioantes;
      const porcentaje = this.crearAnuncio.producto_descuento;
      const descuento = (valor * porcentaje / 100);

      this.crearAnuncio.producto_precioahora = (valor - descuento);
      this.crearAnuncio.producto_codigo = new Date().getTime() + new Date().getUTCMilliseconds().toString();
      this.crearAnuncio.sucursal_codigo = this.local.codigo_sucursal;
      this.crearAnuncio.usuario_codigo = this.local.usuario_codigo;
      this.crearAnuncio.categoria_codigo = this.local.categoria_codigo;

      this.crudService.createDatos(this.tablaProducto, this.crearAnuncio)
         .then(respuesta => {

            Swal.fire(
               'Correcto!',
               'El anuncio se creo Satisfactoriamente',
               'success'
            );

            this.limpiarCampo();
         }).catch(error => {

         Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Valida los datos por favor!',
            footer: `<a href>${error}</a>`
         });

      });
   }



   async actualizarAnuncio() {

      let valor: number;
      let porcentaje: number;
      let descuento: number;
      let valorFinal: number;
      valor = this.crearAnuncio.producto_precioantes;
      porcentaje = this.crearAnuncio.producto_descuento;
      descuento = (valor * porcentaje / 100);
      valorFinal = valor - descuento;

      this.crearAnuncio.producto_precioahora = valorFinal;
      this.crearAnuncio.producto_codigo = new Date().getTime() + new Date().getUTCMilliseconds().toString();

      this.crearAnuncio.sucursal_codigo = this.local.codigo_sucursal;
      this.crearAnuncio.usuario_codigo = this.local.usuario_codigo;


      this.crudService.update(this.tablaProducto, this.idColeccion, this.crearAnuncio)
         .then(respuesta => {

            Swal.fire('Correcto!',
               'El anuncio fue modificado Satisfactoriamente',
               'success');
            this.mostarrAnuncio();
            // this.limpiarCampo();
         }).catch(error => {

         Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Valida los datos por favor!',
            footer: `<a href>${error}</a>`
         });

      });
   }



   async limpiarCampo() {
      this.crearAnuncio = {
         producto_codigo: '',
         producto_nombre: '',
         producto_descripcion: '',
         producto_descuento: 0,
         producto_imagen: '',
         producto_precioahora: 0,
         producto_precioantes: 0,
         sucursal_codigo: '',
         categoria_codigo: null,
         usuario_codigo: '',
         producto_activo: '',
      };

   }



   async mensajeAlertas() {

      Swal.fire({
         title: '¿Validar los datos?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Crear!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {
            const respuesta = this.crearAnuncios();
         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'El registro fue cancelado :)',
               'error'
            );
         }
      });
   }



   async mensajeAlertasUpdate() {
      Swal.fire({
         title: '¿Validar los datos?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Actualizar!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

            // tslint:disable-next-line: prefer-const
            this.actualizarAnuncio();

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'La actualizaciòn fue cancelado :)',
               'error'
            );
         }
      });
   }

}
