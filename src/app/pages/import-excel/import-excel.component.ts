import {Component, OnInit} from '@angular/core';
import readXlsxFile from 'read-excel-file';
import {CrudService} from '../../services/crud.service';

@Component({
   selector: 'app-import-excel',
   templateUrl: './import-excel.component.html',
   styleUrls: ['./import-excel.component.scss']
})
export class ImportExcelComponent implements OnInit {

   constructor(private crudService: CrudService) {
      /*
      this.crudService.read('tb_pregunta').then(value => {
         value.subscribe(value1 => {
            console.log(value1);
         });
      });

      this.crudService.read('tb_respuesta').then(value => {
         value.subscribe(value1 => {
            console.log(value1);
         });
      });
      */
   }



   ngOnInit() {
      const input = document.getElementById('input');

      input.addEventListener('change', () => {
         readXlsxFile(input[`files`][0]).then(rows => {
            if (rows !== null) {
               const data = rows as Array<object>;

               let counterQuestions = 0;
               let counterResponses = 0;

               data.forEach(async value => {

                  const question = {
                     pregunta_codigo      : value[0],
                     pregunta_descripcion : value[1],
                     pregunta_tiempo      : 10,
                     pregunta_tipo        : null
                  };

                  console.log('Question: ' + counterQuestions++, question);

                  this.crudService.createDatos('tb_pregunta', question);

                  for (let i = 2; i < 5; i++) {
                     const response = {
                        pregunta_codigo         : value[0],
                        respuesta_codigo        : (value[0] + (i - 1)),
                        respuesta_descripcion   : value[i],
                        respuesta_puntaje       : (i === 2 ? 20 : 0),
                        respuesta_valor         : (i === 2)
                     };

                     console.log('Response: ' + counterResponses++, response);

                     this.crudService.createDatos('tb_respuesta', response);
                  }
               });
            }
         });
      });
   }

}
