import {Component, OnInit} from '@angular/core';
import {Sucursal} from '../../interfaz/sucursal';
import {CrudService} from '../../services/crud.service';
import {Router, ActivatedRoute} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthenticationService} from 'src/app/services/authentication.service';
import Swal from 'sweetalert2';
import {Observable} from 'rxjs';
import {AngularFireStorage} from '@angular/fire/storage';
import {finalize} from 'rxjs/operators';

@Component({
   selector: 'app-sucursal',
   templateUrl: './sucursal.component.html',
   styleUrls: ['./sucursal.component.scss']
})
export class SucursalComponent implements OnInit {

   sucursalData: Sucursal;
   tablaSucursal = '/tb_sucursal';
   idParametro: any;
   idColeccion: any;
   rutaImagen: any;
   imagenPerfil: any;
   rutaImagenFi: any;
   obtenerurlImagen: any;
   local: any;
   tablamisCategoria = '/tb_usudetalle';
   listadoMiCategorias: any;
   campodetalleC = 'usuario_codigo';




   constructor(
      private crudService: CrudService,
      private router: Router,
      private parametro: ActivatedRoute,
      private imagenFirebase: AngularFireStorage) {

      this.idParametro = parametro.snapshot.paramMap.get('id');

      if (this.idParametro) {
         this.mostarrSucursal(this.idParametro);
      } else {
         this.sucursalData = {};
      }
   }




   ngOnInit() {

      this.local = JSON.parse(localStorage.getItem('user_data'));
      this.listarCategorias(this.local.usuario_codigo);

      this.sucursalData = {
         sucursal_apertura: null,
         sucursal_cierre: null,
         sucursal_clave: null,
         sucursal_codigo: null,
         sucursal_descriptcion: null,
         sucursal_estado: '',
         sucursal_imagen: null,
         sucursal_nombre: null,
         sucursal_telefono: null,
         sucursal_ubicacion: null,
         sucursal_usuario: null,
         usuario_codigo: null,
         categoria_codigo: '',
         // tslint:disable-next-line:max-line-length
         sucursal_fechacreacion: new Date().toISOString().slice(0, 10) + '/' + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds(),
      };
   }




   async mostarrSucursal(value) {
      this.crudService.readGeneral(this.tablaSucursal, 'sucursal_codigo', value).then(respuesta => {
         respuesta.subscribe(susc => {
            this.idColeccion = susc[0].payload.doc.id;
            this.sucursalData = susc[0].payload.doc.data();
         });
      }).catch(error => {
         console.error('error en la consulta ' + JSON.stringify(error));
      });
   }

   async listarCategorias(value) {
      this.crudService.readGeneral1(this.tablamisCategoria, this.campodetalleC, value).then(respuesta => {
         respuesta.subscribe(suscr => {
            this.listadoMiCategorias = suscr;
         });

      }).catch(error => {
         console.error('error en a respuesta ' + JSON.stringify(error));
      });
   }


   uploadAvatar() {
      document.getElementById('upload-avatar').click();
   }

   async subirFotos(event) {
      /*
      this.imagenPerfil = event.target.files[0];
      this.rutaImagen = `sucursales/${this.sucursalData.sucursal_codigo}`;
      const ref = this.imagenFirebase.ref(this.rutaImagen);
      const tark = this.imagenFirebase.upload(this.rutaImagen, this.imagenPerfil);

      tark.snapshotChanges().pipe(() => this.obtenerurlImagen = ref.getDownloadURL()
      ).subscribe(respuestas => {

         this.rutaImagenFi = respuestas;
         this.sucursalData.sucursal_imagen = this.rutaImagenFi;

      });
      */


      try {

         this.imagenPerfil = event.target.files[0];
         this.rutaImagen = `sucursales/${this.sucursalData.sucursal_codigo}`;
         const ref = this.imagenFirebase.ref(this.rutaImagen);
         const task = this.imagenFirebase.upload(this.rutaImagen, this.imagenPerfil);

         task.snapshotChanges().pipe(
            finalize(async () => {
               ref.getDownloadURL().subscribe(value => {
                  this.sucursalData.sucursal_imagen = value;
               });
            })
         ).subscribe();

      } catch (e) {
         console.error(e);
      }
   }


   async updateSucursal() {
      try {
         this.crudService.update(this.tablaSucursal, this.idColeccion, this.sucursalData).then(respuesta => {
            Swal.fire('Correcto!',
               'La sucursal fue modificada Satisfactoriamente',
               'success');
            });
      } catch (error) {
         Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Valida los datos por favor!',
            footer: `<a href>${error}</a>`
         });

         console.error(JSON.stringify(error));
      }
   }

   async crearSucursales() {
      this.sucursalData.usuario_codigo = this.local.usuario_codigo;
      this.sucursalData.sucursal_codigo = new Date().getTime().toString();
      try {
         this.crudService.createDatos(this.tablaSucursal, this.sucursalData).then(async respuesta => {
            Swal.fire('Correcto!',
               'La sucursal se creo Satisfactoriamente',
               'success');

            await this.limpiarDatos();

         }).catch(error => {
            Swal.fire({
               type: 'error',
               title: 'Oops...',
               text: 'Valida los datos por favor!',
               footer: `<a href>${error}</a>`
            });
         });
      } catch (error) {
         console.error('error en la respuesta ' + JSON.stringify(error));
      }
   }

   async limpiarDatos() {
      this.sucursalData = {
         sucursal_apertura: '',
         sucursal_cierre: '',
         sucursal_clave: '',
         sucursal_codigo: '',
         sucursal_descriptcion: '',
         sucursal_estado: '',
         sucursal_imagen: '',
         sucursal_nombre: '',
         sucursal_telefono: '',
         sucursal_ubicacion: '',
         sucursal_usuario: '',
         usuario_codigo: '',
      };

   }

   async mensajeAlertas() {
      Swal.fire({
         title: '¿Validar los datos?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Registrarte!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

            // tslint:disable-next-line: prefer-const
            let respuesta = this.crearSucursales();

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'El registro fue cancelado :)',
               'error'
            );
         }
      });
   }

   async mensajeAlertasUpdate() {
      Swal.fire({
         title: '¿Validar los datos?',
         text: 'Seleccione la opcion que va a realizar!',
         type: 'warning',
         showCancelButton: true,
         confirmButtonText: 'Registrarte!',
         cancelButtonText: 'Cancelar'
      }).then((result) => {
         if (result.value) {

            // tslint:disable-next-line: prefer-const
            let respuesta = this.updateSucursal();

         } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
               'Cancelado',
               'La actualizaciòn fue cancelado :)',
               'error'
            );
         }
      });
   }
}
