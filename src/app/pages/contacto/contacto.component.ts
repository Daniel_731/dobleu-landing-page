import { Component, OnInit } from '@angular/core';
import { Contacto } from '../../interfaz/contacto';
import { CrudService } from 'src/app/services/crud.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {
  contactos: Contacto;
  local: any;
  tablaContacto = '/tb_contacto';

  constructor(
    private crudService: CrudService,
    public afAuth: AngularFireAuth,
    public authService: AuthenticationService,
    private router: Router,
    private parametro: ActivatedRoute,
    private imagenFirebase: AngularFireStorage,
  ) {

    this.local = JSON.parse(localStorage.getItem('user_data'));
    console.log(this.local);
  }

  ngOnInit() {

    this.contactos = {
      contacto_codigo: null,
      contacto_contenido: null,
      contacto_email: null,
      contacto_titulo: null,
      usuario_codigo: null,
    };

    this.contactos.contacto_email = this.local.usuario_email;
  }

  async crearContactos() {
    this.contactos.contacto_codigo = new Date().getTime().toString();
    this.contactos.usuario_codigo = this.local.usuario_codigo;

    this.crudService.createDatos(this.tablaContacto, this.contactos)
      .then(respuesta => {

        Swal.fire('Correcto!',
          'Correo fue enviado Correctamente',
          'success');
        this.limpiarContactos();
      }).catch(error => {

        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Valida los datos por favor!',
          footer: `<a href>${error}</a>`
        });

      });
  }

  async limpiarContactos() {
    this.contactos = {
      contacto_codigo: '',
      contacto_contenido: '',
      contacto_titulo: '',
      usuario_codigo: '',
    };
  }

  async mensajeEnviarCorreo() {

    Swal.fire({
      title: '¿Validar los datos?',
      text: 'Seleccione la opcion que va a realizar!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Enviar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {

        this.crearContactos();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'El registro fue cancelado :)',
          'error'
        );
      }
    });
  }

}
