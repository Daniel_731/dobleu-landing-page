import {Component, OnInit} from '@angular/core';
import {Sucursal} from 'src/app/interfaz/sucursal';
import {Observable} from 'rxjs';
import {CrudService} from 'src/app/services/crud.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {Router, ActivatedRoute} from '@angular/router';
import {AngularFireStorage} from '@angular/fire/storage';
import {Usuario} from 'src/app/interfaz/usuario';
import {UsuarioCategoria} from 'src/app/interfaz/usuario-categoria';
import Swal from 'sweetalert2';
import { async } from 'q';

@Component({
   selector: 'app-usuario',
   templateUrl: './usuario.component.html',
   styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
   sucursalData: Sucursal;
   usuario: Usuario;
   tablaUsuario = '/tb_usuario';
   tablaCategoria = '/tb_categoria';
   tablaDetalleUsu = 'tb_usudetalle';

   idParametro: any;
   idColeccion: any;
   idUsuario: any;
   rutaImagen: any;
   imagenPerfil: any;
   public rutaImagenFi: any;
   porcentajeCarga: Observable<number>;
   obtenerurlImagen: any;
   usuarioImagen: any;
   foto: any;
   local: any;
   dropdownList = [];
   selectedItems = [];
   dropdownSettings = {};

   constructor(
      private crudService: CrudService,
      public afAuth: AngularFireAuth,
      public authService: AuthenticationService,
      private router: Router,
      private parametro: ActivatedRoute,
      private imagenFirebase: AngularFireStorage,
   ) {

   }

   ngOnInit() {

      this.selectedItems = [
         {item_id: 3, item_text: 'Pune'},
         {item_id: 4, item_text: 'Navsari'}
      ];


      this.dropdownSettings = {
         singleSelection: false,
         idField: 'categoria_codigo',
         textField: 'categoria_nombre',
         itemsShowLimit: 3,
         allowSearchFilter: true
      };

      this.usuario = {
         usuario_alias: null,
         usuario_celular: null,
         usuario_clave: null,
         usuario_codigo: null,
         usuario_coleccion: null,
         usuario_direccion: null,
         usuario_email: null,
         usuario_fechamodificacion: new Date().toISOString().slice(0, 10)+'/'+new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds(),
         usuario_nombre: null,
         usuario_telefono: null,
         usuario_fechacreacion: null,
         usuario_tipologin: null,
         usuario_tipo: null,
         usuario_documento: null,
      };
      this.local = JSON.parse(localStorage.getItem('user_data'));
      console.log(this.local);
      this.mostarrSucursal(this.local[`usuario_codigo`]);
      this.listarCategorias();
      this.listarDetalleUsuario(this.local[`usuario_codigo`]);

   }


   uploadAvatar() {
      document.getElementById('upload-avatar').click();
   }

   async subirFotos(event) {

      this.imagenPerfil = event.target.files[0];
      this.rutaImagen = `fotos_perfil/${this.usuario.usuario_codigo}`;
      const ref = this.imagenFirebase.ref(this.rutaImagen);
      const tark = this.imagenFirebase.upload(this.rutaImagen, this.imagenPerfil);

      tark.snapshotChanges().pipe(() => this.obtenerurlImagen = ref.getDownloadURL()
      ).subscribe(respuestas => {

         this.rutaImagenFi = respuestas;
         this.usuario.usuario_imagen = this.rutaImagenFi;
         console.log(this.tablaUsuario, this.idColeccion,  this.usuario);
         this.crudService.update(this.tablaUsuario, this.idColeccion,  this.usuario);

      });
   }


   async mostarrSucursal(value) {

      console.log(value);
      this.crudService.readGeneral(this.tablaUsuario, 'usuario_codigo', value)
         .then(respuesta => {
            respuesta.subscribe(susc => {
               this.idColeccion = susc[0].payload.doc.id;
               this.usuario = susc[0].payload.doc.data();
               console.log(this.idColeccion);
               console.log(this.usuario);

            });
         }).catch(error => {
         console.log('error en la consulta ' + JSON.stringify(error));

      });
   }

   onItemSelect(item: any) {
      console.log(item);
      // this.crearCategorias();
   }

   onSelectAll(items: any) {
      console.log(items);
      // this.crearCategorias();
   }

   async listarCategorias() {
      this.crudService.read(this.tablaCategoria).then(respuesta => {
         respuesta.subscribe(suscr => {
            this.dropdownList = suscr;
         });

      }).catch(error => {
         console.log('error en a respuesta ' + JSON.stringify(error));
      });
   }

   async listarDetalleUsuario(value) {
      this.crudService.readGeneral(this.tablaDetalleUsu, 'usuario_codigo', value).then(respuesta => {
         respuesta.subscribe(suscr => {
            this.selectedItems = suscr;
            console.log(this.selectedItems);
         });

      }).catch(error => {
         console.log('error en a respuesta ' + JSON.stringify(error));
      });
   }


   // async listarDetalleUsuarioCategoria() {

   //    this.crudService.readUsuarios(this.tablaDetalleUsu, 'usuario_codigo', this.usuario.usuario_codigo).then(respuesta => {
   //       respuesta.subscribe(suscr => {
   //          this.selectedItems = suscr;
   //          for (let index = 0; index < suscr.length; index++) {
   //             const element = suscr[index];
   //             let categoriaDetalle = element[index].payload.doc.id;
   //             console.log(categoriaDetalle);
   //              this.crudService.delete(this.tablaDetalleUsu, categoriaDetalle);
   //          }

   //       });

   //    }).catch(error => {
   //       console.log('error en a respuesta ' + JSON.stringify(error));
   //    });

   // }

   async actualizarUsuario() {

      console.log(this.tablaUsuario, this.idColeccion, this.usuario);
      try {
        this.crudService.update(this.tablaUsuario, this.idColeccion, this.usuario)
          .then(respuesta => {
            Swal.fire('Correcto!',
              'Usuario actualizados Corectamente',
              'success');
            console.log(respuesta);
            this.crearCategorias();

          });
      } catch (error) {

        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Valida los datos por favor!',
          footer: `<a href>${error}</a>`
        });

        console.log(JSON.stringify(error));
      }

   }


   async crearCategorias() {

      console.log(this.selectedItems);

      this.selectedItems.forEach(respuesta => {

         setTimeout(() => {

            respuesta[`usudet_codigo`] = new Date().getTime() + new Date().getUTCMilliseconds();
            respuesta[`usuario_codigo`] = this.usuario.usuario_codigo;
            respuesta[`categoria_codigo`] = respuesta[`categoria_codigo`];
            try {
               this.crudService.createDatos(this.tablaDetalleUsu, respuesta).then(respue => {
                  console.log('detalle ready');
               }).catch(error => {

               });
            } catch (error) {

            }
         }, 3);
      });
   }


   async mensajeAlertasUpdate () {


    Swal.fire({
      title: '¿Validar los datos?',
      text: 'Seleccione la opcion que va a realizar!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Registrarte!',
      cancelButtonText: 'Cancelar'
    }).then(async ( result) => {
      if (result.value) {

        // this.listarDetalleUsuarioCategoria();
         await this.actualizarUsuario();

      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'El registro fue cancelado :)',
          'error'
        );
      }
    });
   }

}
