import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../services/crud.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Usuario } from 'src/app/interfaz/usuario';
import { ConditionalExpr } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
   selector: 'app-login',
   templateUrl: './login.component.html',
   styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
   usuario: Usuario;
   tablaUser = '/tb_usuario';
   usuarioLocal: any;
   campo = 'usuario_codigo';
   local: any;

   constructor(
      private authService: AuthenticationService,
      private crudService: CrudService,
      private router: Router,
   ) {}

   ngOnInit() {
      this.usuario = {
         usuario_codigo: null,
         usuario_coleccion: null,
         usuario_nombre: null,
         usuario_email: null,
         usuario_clave: null,
         usuario_longitud: 111111111,
         usuario_latitud: 1111111111,
         usuario_tipo: 'Persona',
         usuario_imagen: 'https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y',
      };
   }


   async signInGoogle() {
      console.log('signInGoogle');
   }

   async signInFacebook() {
      console.log('signInFacebook');

   }

   async usuarioLogin() {
      await this.authService.loginNormal(this.usuario).then(user => {
         if (user) {
            this.usuario.usuario_codigo = user.user.uid;

            this.crudService.readUsuarios(this.tablaUser, this.campo, this.usuario.usuario_codigo).then(respuesta => {
               respuesta.subscribe(res => {
                  res.forEach(async value => {
                     const data = res[0].payload.doc.data();
                     this.usuarioLocal = data as Usuario;
                     console.log(this.usuarioLocal);
                     localStorage.setItem('user_data', JSON.stringify(this.usuarioLocal));
                     this.router.navigateByUrl('/home');
                  });
               });
            });

         } else {
            this.traerLoginSucursal();
         }

      }).catch(error => {
         console.log(error);
      });
   }



   async traerLoginSucursal() {
      this.authService.loginBranchOffice('tb_sucursal', this.usuario.usuario_email, this.usuario.usuario_clave).then(respuesta => {
         respuesta.subscribe(async ressucursal => {
            if (ressucursal.length > 0) {

               this.usuario.usuario_tipo = 'Sucursal';
               this.usuario.usuario_codigo = ressucursal[0][`usuario_codigo`];
               this.usuario.usuario_nombre = ressucursal[0][`sucursal_nombre`];
               this.usuario.usuario_imagen = ressucursal[0][`sucursal_imagen`];
               this.usuario.categoria_codigo = ressucursal[0][`categoria_codigo`];
               this.usuario.codigo_sucursal = ressucursal[0][`sucursal_codigo`];

               localStorage.setItem('user_data', JSON.stringify(this.usuarioLocal));
               this.router.navigateByUrl('/home');

            } else {

               alert('El usuario o la contraseña son incorrectos!');
            }

         });
      });
   }

}
