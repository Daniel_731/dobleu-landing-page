import {Time} from '@angular/common';

export interface Usuario {
   usuario_codigo?: string; // codigo o id
   usuario_email?: string; // correo de usuario o empres
   usuario_nombre?: string;
   usuario_documento?: string;
   usuario_puntaje?: number;
   usuario_clave?: string; // contraseña
   usuario_tipo?: string; // si es empresa o usuario
   usuario_celular?: string; // numero celular
   usuario_alias?: string;
   categoria_codigo?: number; // viene de la tabla categoria
   usuario_telefono?: string; // numero fijo
   usuario_tipologin?: string; // creado normal ,por facebook, o google
   usuario_direccion?: number; // donde esta ubicado
   usuario_longitud?: number; // para saber la ubicacion
   usuario_latitud?: number; // para saber la ubicacion
   usuario_imagen?: string;   // La imagen de perfil
   usuario_sexo?: string;
   usuario_facebook?: string;
   usuario_instagram?: string;
   usuario_twitter?: string;
   usuario_coleccion?: string;
   codigo_sucursal?: string;
   usuario_horaapertura?: Time;
   usuario_estado?: boolean; // para saber si esta inactivo o activo
   usuario_horacierre?: Time;
   usuario_fechacreacion?: string;
   usuario_fechamodificacion?: string;
}
