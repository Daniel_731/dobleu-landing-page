export interface UsuarioCategoria {
   usudet_codigo?: string;
   usuario_codigo?: string;
   categoria_codigo?: string;
   categoria_nombre?: string;
}
