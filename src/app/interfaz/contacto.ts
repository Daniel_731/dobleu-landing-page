export interface Contacto {
    contacto_codigo?: string;
    contacto_contenido?: string;
    contacto_email?: string;
    contacto_titulo?: string;
    usuario_codigo?: string;
}
