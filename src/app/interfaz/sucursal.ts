export interface Sucursal {
    sucursal_codigo?: string;
    usuario_codigo?: string;
    sucursal_nombre?: string;
    sucursal_telefono?: string;
    sucursal_ubicacion?: string;
    sucursal_usuario?: string;
    sucursal_clave?: string;
    sucursal_apertura?: string;
    sucursal_cierre?: string;
    sucursal_estado?: string;
    sucursal_imagen?: string;
    categoria_codigo?: string;
    sucursal_descriptcion?: string;
    sucursal_fechacreacion?: string;
}
