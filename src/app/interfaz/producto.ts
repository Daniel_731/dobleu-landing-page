export interface Producto {
   producto_codigo?: string;
   producto_nombre?: string;
   producto_descripcion?: string;
   producto_descuento?: number;
   producto_imagen?: string;
   producto_precioahora?: number;
   producto_precioantes?: number;
   sucursal_codigo?: string;
   categoria_codigo?: number;
   usuario_codigo?: string;
   producto_activo?: string;
}
