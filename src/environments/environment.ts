// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDKuypUhLRkLs3KYaLjEXa6h9S01U3iUNk',
    authDomain: 'dobleu-b3bbe.firebaseapp.com',
    databaseURL: 'https://dobleu-b3bbe.firebaseio.com',
    projectId: 'dobleu-b3bbe',
    storageBucket: 'dobleu-b3bbe.appspot.com',
    messagingSenderId: '217747230762',
    appId: '1:217747230762:web:057ed7954a6170a1ed7353',
    measurementId: 'G-58702D9VB1'
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
